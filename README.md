# Images
In progress

# Updates

# v3

- angle about 8.5 degrees
- front height about 12 mm

**Mistake**: hole for reset switch in the wrong place

# v4

- trying to save material, not tested
- same mistake as v3

# v5

- after printing v3 I added 0.5mm of horizontal space for PCB in every direction
- corrected mistake of v3 and v4
- angle changed to 10 degrees

**Mistake**: additional space was not necessary, it was an error from the previous print

# v6

- reduced space
- reduced height to 10mm in the front
- removed 2 front holes for screws (3 are enough)
- 2 versions: chamfered and filleted (round edges)
- not printed yet!

**Note to self:** All designs were made using [Onshape](https://www.onshape.com/).